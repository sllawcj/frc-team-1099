/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.wpi.first.wpilibj.templates.subsystems;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.Talon;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.templates.RobotMap;
import edu.wpi.first.wpilibj.templates.commands.pickerupper.Stop;

/**
 *
 * @author DiscoTech
 */
public class PickerUpper extends Subsystem
{
    DoubleSolenoid inOut;
    Talon spinner;
    double speed;
    
    public PickerUpper(){
        // inOut = new Solenoid(RobotMap.INOUT);
        inOut = new DoubleSolenoid(RobotMap.PICKUP_EXTEND, RobotMap.PICKUP_RETRACT);
        
        spinner = new Talon(RobotMap.PICKERUPPER);
        speed = 1;
    }
    
    public void extend(){
        inOut.set(DoubleSolenoid.Value.kForward);
    }
    
    public void retract(){
        inOut.set(DoubleSolenoid.Value.kReverse);
    }
    
    public void spinIn(){
        spinner.set(speed);
    }
    
    public void spinOut(){
        spinner.set(-speed);
    }
    
    public void speedUp(){
        if(speed<1)speed+=.05;
        SmartDashboard.putNumber("Pickup Speed", speed);
    }
    
    public void slowDown(){
        if(speed>1)speed-=.05;
        SmartDashboard.putNumber("Pickup Speed", speed);
    }
    
    public void stop(){
        spinner.set(0);
    }

    public void initDefaultCommand()
    {
        setDefaultCommand(new Stop());
    }
}
