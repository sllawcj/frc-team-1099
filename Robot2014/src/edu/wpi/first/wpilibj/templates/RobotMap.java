package edu.wpi.first.wpilibj.templates;

/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap {
    //PWM Channels
    public static final int LDRIVE = 1;
    public static final int RDRIVE = 2;
    public static final int PICKERUPPER = 3;
    public static final int WINCHER = 4;
    
    //Solenoid Channels
    public static final int PICKUP_EXTEND = 1;
    public static final int PICKUP_RETRACT = 2;
    public static final int CATAPULT_RELEASE = 3;
    public static final int CATAPULT_LOCK = 4;
    
   //Digital Input
    public static final int CATAPULT_BACK = 1;
    public static final int PRESSURE_SWITCH = 14;
    public static final int RELAY = 3;
  
    //Joystick Ports
    public static final int LEFTSTICK = 1;
    public static final int RIGHTSTICK = 2;
    public static final int GAMEPAD = 3;
    
    //JoystickButtons
    public static final int EXTEND = 2;
    public static final int SPININ = 5;
    public static final int SPINOUT = 6;
    public static final int SlowDown = 3;
    public static final int SpeedUp = 4;
    
}
 