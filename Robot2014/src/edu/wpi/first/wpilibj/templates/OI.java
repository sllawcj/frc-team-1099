
package edu.wpi.first.wpilibj.templates;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.Button;
import edu.wpi.first.wpilibj.buttons.DigitalIOButton;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.templates.commands.pickerupper.Extend;
import edu.wpi.first.wpilibj.templates.commands.camera.IsHot;
import edu.wpi.first.wpilibj.templates.commands.pickerupper.Retract;
import edu.wpi.first.wpilibj.templates.commands.pickerupper.SlowDown;
import edu.wpi.first.wpilibj.templates.commands.pickerupper.SpeedUp;
import edu.wpi.first.wpilibj.templates.commands.pickerupper.SpinIn;
import edu.wpi.first.wpilibj.templates.commands.pickerupper.SpinOut;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI {
    public static Joystick leftStick;
    public static Joystick rightStick;
    public static Joystick gamepad;
    
    public OI(){
        leftStick = new Joystick(RobotMap.LEFTSTICK);
        rightStick = new Joystick(RobotMap.RIGHTSTICK);
        gamepad = new Joystick(RobotMap.GAMEPAD);
        
        Button extend = new JoystickButton(gamepad, RobotMap.EXTEND);
        Button spinin = new JoystickButton(gamepad, RobotMap.SPININ);
        Button spinout = new JoystickButton(gamepad, RobotMap.SPINOUT);
        Button SlowDown = new JoystickButton(gamepad, RobotMap.SlowDown);
        Button SpeedUp = new JoystickButton(gamepad, RobotMap.SpeedUp);
        
        
        
        extend.whenPressed(new Extend());
        extend.whenReleased(new Retract());
        spinin.whileHeld(new SpinIn());
        spinout.whileHeld(new SpinOut());
        SlowDown.whenPressed(new SlowDown());
        SpeedUp.whenPressed(new SpeedUp());
        
        
        SmartDashboard.putData("Test Camera", new IsHot());
        SmartDashboard.putNumber("Left Multiplier", 1.0);
        SmartDashboard.putNumber("Right Multiplier", 1.0);
        
    }
}

