/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.wpi.first.wpilibj.templates.subsystems;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.Talon;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.templates.RobotMap;

/**
 *
 * @author DiscoTech
 */
public class Catapult extends Subsystem
{
    
    Talon wincher;
    DoubleSolenoid release;
    DigitalInput pulledBack;
    DoubleSolenoid gearMover;
    private DoubleSolenoid.Value DoubleSolenoid;
    
    
    public Catapult(){
        wincher = new Talon(RobotMap.WINCHER);
        release = new DoubleSolenoid(RobotMap.CATAPULT_RELEASE, RobotMap.CATAPULT_LOCK);
    }
    public void initDefaultCommand()
    {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }
    
    public void pullBack(){
        wincher.set(.5);
        while(pulledBack.get());
        wincher.set(0);
        // release.set(DoubleSolenoid.Value.k);
    }
    
    public boolean getSwitch(){
        return pulledBack.get();
    }
    
    public void fire() throws InterruptedException{
        
        
    }
    
    public void gearMove(){
       
    }
}
