/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.wpi.first.wpilibj.templates.commands.drive;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.templates.commands.CommandBase;

/**
 *
 * @author DiscoTech
 */
public class AutoDrive extends CommandBase
{

    double speed = 0.0;
    double lMult =1.0;
    double rMult =1.0;
    
    
    public AutoDrive(double speed)
    {
        requires(drive);
        this.speed = speed;
        lMult = SmartDashboard.getNumber("Left Multiplier", lMult);
        rMult = SmartDashboard.getNumber("Right Multiplier", rMult); 
                
    }
    
    protected void initialize()
    {
    }

    protected void execute()
    {
        drive.drive(speed,lMult,rMult);
    }

    protected boolean isFinished()
    {
        return false;
    }

    protected void end()
    {
        
    }

    protected void interrupted()
    {
        
    }
}
