/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package team1099.smartdashboard.extension;

import edu.wpi.first.smartdashboard.camera.WPICameraExtension;
import edu.wpi.first.smartdashboard.robot.Robot;
import edu.wpi.first.wpijavacv.WPIBinaryImage;
import edu.wpi.first.wpijavacv.WPIColor;
import edu.wpi.first.wpijavacv.WPIColorImage;
import edu.wpi.first.wpijavacv.WPIContour;
import edu.wpi.first.wpijavacv.WPIImage;
import edu.wpi.first.wpijavacv.WPIPoint;
import edu.wpi.first.wpijavacv.WPIPolygon;
import edu.wpi.first.wpilibj.networktables.NetworkTable;

/**
 *
 * @author DiscoTech
 */
public class RedFilter extends WPICameraExtension
{   
    NetworkTable table;

    @Override
    public void init()
    {
        super.init();

        table = (NetworkTable)Robot.getTable( "camera" );
    }

    @Override
    public WPIImage processImage(WPIColorImage rawImage)
    {
        
        // cut off under this red level
        WPIBinaryImage redLo = rawImage.getRedChannel().getThresholdInverted(200);

        // over this green
        WPIBinaryImage greenHi = rawImage.getGreenChannel().getThreshold(128);

        // under this blue
        WPIBinaryImage blueLo = rawImage.getBlueChannel().getThresholdInverted(200);

        // The only thing that is ON (white) on all three channels
        // is white itself
        WPIBinaryImage whiteBin = redLo;
        whiteBin = whiteBin.getAnd(greenHi);
        whiteBin = whiteBin.getAnd(blueLo);

        try
        {
            whiteBin.erode(4);
            whiteBin.dilate(8);
            // finalBin.invert();
        } catch (Exception e)
        {
            // Where would this go?   
        }

        WPIContour[] contours = whiteBin.findContours();
        rawImage.drawContours(contours, WPIColor.GREEN, 2);

        WPIPolygon bestGuess = null;
        int bestArea = 0;

        for (WPIContour c : contours)
        {
            WPIPolygon p = c.approxPolygon(45);

            double ratio = 1.0 * p.getWidth() / p.getHeight();
            if (0 < ratio && ratio < 100
                    && p.isConvex()
                    && p.getNumVertices() == 4)
            {
                rawImage.drawPolygon(p, WPIColor.YELLOW, 4);

                // let's choose the biggests as the "best guess"
                if (p.getArea() > bestArea)
                {
                    bestGuess = p;
                    bestArea = p.getArea();
                }
            }
        }

        boolean onTarget = false;
        int hOffset = 0;
        int vOffset = 0;

        if (bestGuess != null)
        {

            WPIPoint[] vertex = bestGuess.getPoints();

            int hCenter = (vertex[0].getX() + vertex[1].getX()) / 2;
            int vCenter = (vertex[0].getY() + vertex[3].getY()) / 2;

            hOffset = hCenter - rawImage.getWidth() / 2;
            vOffset = vCenter - rawImage.getHeight() / 2;

            if ((-25 < hOffset && hOffset < 25)
                    && (-25 < vOffset && vOffset < 25))
            {
                rawImage.drawPolygon(bestGuess, WPIColor.RED, 8);
                onTarget = true;
            }

        }


        boolean found = (bestGuess != null);
        
        table.putBoolean("found", found);
        table.putBoolean("onTarget", onTarget);
        table.putNumber("hOffset", hOffset);
        table.putNumber("vOffset", vOffset);
        
        return rawImage;
    }
}
