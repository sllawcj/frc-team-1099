package edu.wpi.first.wpilibj.templates.commands;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import edu.wpi.first.wpilibj.templates.commands.drive.AutoDrive;
import edu.wpi.first.wpilibj.command.CommandGroup;
import edu.wpi.first.wpilibj.templates.commands.pickerupper.SpinOut;

/**
 *
 * @author DiscoTech
 */
public class AutoCommand extends CommandGroup
{
    
    public AutoCommand()
    {
        addSequential(new AutoDrive(.55), 3.0 );
        addSequential(new AutoDrive(.35), 3.0 );
        addSequential( new SpinOut(), 2.0 );
    }

}
