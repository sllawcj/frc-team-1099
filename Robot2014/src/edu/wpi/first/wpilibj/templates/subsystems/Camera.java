/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.wpi.first.wpilibj.templates.subsystems;

import edu.wpi.first.wpilibj.camera.AxisCamera;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.image.BinaryImage;
import edu.wpi.first.wpilibj.image.ColorImage;
import edu.wpi.first.wpilibj.image.CriteriaCollection;
import edu.wpi.first.wpilibj.image.NIVision;
import edu.wpi.first.wpilibj.image.ParticleAnalysisReport;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 * @author DiscoTech
 */
public class Camera extends Subsystem
{
    CriteriaCollection cc = new CriteriaCollection();
    AxisCamera camera = AxisCamera.getInstance("10.10.99.11");
    //FREE ALL CREATED IMAGES
    ColorImage image;
    BinaryImage green;
    BinaryImage bigObjects;
    BinaryImage filled;
    BinaryImage filtered;
    boolean hotFound;
    ParticleAnalysisReport hot;
    
    public Camera(){
        cc.addCriteria(NIVision.MeasurementType.IMAQ_MT_BOUNDING_RECT_WIDTH, 30, 400, false);
        cc.addCriteria(NIVision.MeasurementType.IMAQ_MT_BOUNDING_RECT_HEIGHT, 40, 400, false);
    }
    
    public boolean isHot(){
        if(!camera.freshImage() || true){
            hotFound = false;
             ParticleAnalysisReport[] reports = filter();
            //do stuff with reports
            for(int i = 0;i<reports.length;i++){
                ParticleAnalysisReport report = reports[i];
                if(report.boundingRectWidth/report.boundingRectHeight>4&&
                   report.boundingRectWidth/report.boundingRectHeight<7)
                {
                    hotFound = true;
                    hot = report;
                }
                SmartDashboard.putBoolean("Hot?", hotFound);
                SmartDashboard.putNumber("Aspect Ratio", report.boundingRectWidth/1.0*report.boundingRectHeight);
            }
        }
        freeImages();
        return hotFound;
    }
    
    public int distanceTo(){
        int distance = 0;
        ParticleAnalysisReport[] reports = filter();
        
        for(int i = 0;i<reports.length;i++){
            ParticleAnalysisReport report = reports[i];
            if(report.boundingRectHeight/report.boundingRectWidth>7&&
               report.boundingRectHeight/report.boundingRectWidth<9){
                distance = (int) (((8/3)*report.imageHeight)/(2*report.boundingRectHeight*Math.tan(37.4))); //gotten from wpilib.screenstepslive.com/s/3120/m/8731/l/90361-identifying-and-processing-the-targets
                break;
            }
        }
        freeImages();
        return distance;
    }
    
    public ParticleAnalysisReport[] filter(){
        try{
            image = camera.getImage();
            green = image.thresholdRGB(230, 255, 230, 255, 210, 255); //white-blue light from flashlight
            bigObjects = green.removeSmallObjects(false, 2);
            filled = bigObjects.convexHull(false);
            filtered = filled.particleFilter(cc);
            
            filtered.write("/tmp/image.bmp");//FTP to cRIO to find this file

            return filtered.getOrderedParticleAnalysisReports();
        }catch(Exception e){e.printStackTrace();}
        return null;
    }
    
    public void freeImages(){
        try{
            image.free();
            green.free();
            bigObjects.free();
            filled.free();
            filtered.free();
            }catch(Exception e){
                e.printStackTrace();
            }
        }

    public void initDefaultCommand()
    {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }
}
