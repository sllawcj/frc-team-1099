/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.wpi.first.wpilibj.templates;

import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.templates.commands.AutoCommand;

/**
 *
 * @author DiscoTech
 */
public class Autonomous
{
    
    private static final AutoCommand auto = new AutoCommand();
    
    public static void init(){
        
        auto.start();//to switch autonomous command, change this line
    }

    public static void periodic(){
        Scheduler.getInstance().run();
}
}
