
package edu.wpi.first.wpilibj.templates.subsystems;

import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.Talon;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.templates.OI;
import edu.wpi.first.wpilibj.templates.RobotMap;
import edu.wpi.first.wpilibj.templates.commands.drive.Go;

/**
 *
 */
public class Drive extends Subsystem {
//    Joystick leftStick;
//    Joystick rightStick;
    Talon left;
    Talon right;
    RobotDrive driver;
    double leftSpeed;
    double rightSpeed;
    
    public Drive(){
//        this.leftStick = leftStick;
//        this.rightStick = rightStick;
        
        left = new Talon(RobotMap.LDRIVE);
        right = new Talon(RobotMap.RDRIVE);
        driver = new RobotDrive(left, right);
    }
    
    private double lStick = 0.0;
    private double rStick = 0.0;
    private final double threshold = 0.1f;
    
    public void go(){
        lStick = OI.leftStick.getRawAxis(2);
        rStick = OI.rightStick.getRawAxis(2);
        SmartDashboard.putNumber("leftStickRaw", lStick);
        SmartDashboard.putNumber("rightStickRaw", rStick);

        
        if(lStick > 0 && lStick > threshold){
            leftSpeed = lStick*lStick;
        }
        else if ( lStick < 0 &&  lStick < -threshold )
        {
            leftSpeed = -lStick * lStick;
        }
        else
        {
            leftSpeed = 0.0f;
        }
        
        if( rStick > 0 && rStick > threshold )
        {
            rightSpeed = rStick * rStick;
        }
        else if ( rStick < 0 && rStick < -threshold )
        {
            rightSpeed = -rStick * rStick;
        }
        else
        {
            rightSpeed = 0.0f;
        }
        SmartDashboard.putNumber("left drive speed", leftSpeed);
        SmartDashboard.putNumber("right drive speed", rightSpeed);
        
        driver.tankDrive(leftSpeed, rightSpeed);
    }

    public void initDefaultCommand() {
        setDefaultCommand(new Go());
    }
    
    public void drive( double speed, double lMult, double rMult)
    {
        // this is used to drive during autonomous
        driver.tankDrive( lMult*speed, rMult*speed );
    }
    
}

